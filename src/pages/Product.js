import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
// import coursesData from '../data/coursesData';

export default function Courses() {
  const [products, setProducts] = useState([]);

  // Checks to see if the mock data was captured.
  // console.log(coursesData);
  // console.log(coursesData[0]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  // const courses = coursesData.map(course => {
  // 	return (
  // 		<CourseCard key={course.id} courseProp={course} />
  // 	)
  // })

  return (
    <>
      <h1>Products</h1>
      {products}
    </>
  );
}
